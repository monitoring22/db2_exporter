# DB2 Server Exporter

Prometheus exporter for DB2 server metrics.

Get DB2 server information:
- Using your own SQL (examples included)
- Using your own naming for exposed metrics
- Using static and/or dynamic labeling
- Serve your metrics as web server or push them to a push gateway

Requires Python >= 3.6

Check also this article:
https://www.epilis.gr/en/blog/2022/04/03/db2-monitoring-prometheus/

## Building and running


### Building

Use git to clone this repository:

```
git clone https://gitlab.com/monitoring22/db2_exporter.git
cd db2_exporter
```

Create the python virtual environment:

```
python -m venv .
```

Install requirements:

```
pip install -r requirements.txt
```

### Running

Create a .env file to set up your environment:

```
DB2DATABASE = 'XXXX'
DB2HOSTNAME = 'xxxx'
DB2PORT = '50000'
DB2PROTOCOL = 'TCPIP'
DB2UID = 'xxxxxxx'
DB2PWD = 'xxxxxxx'
PUSH_GATEWAY = "http://push_gw_host:9091"
JOB = "some_job"
COLLECTOR_INTERVAL = 30
SQL_UP = "select * from sysibm.sysdummy1"
SQL_ENV = "select INST_NAME, SERVICE_LEVEL from SYSIBMADM.ENV_INST_INFO":
```

Run with example YAML config or with your own:
```
python db2_exporter/db2_exporter.py -p 8000 -c example_config/metrics.yml
```

### Testing

Open your browser and point to http://127.0.0.1:8000/metrics

Example response:

```
# HELP db2_status Running a dummy query
# TYPE db2_status gauge
db2_status{Database="XXXDB"} 1.0
# HELP db2_last_check Last time a check job finished
# TYPE db2_last_check gauge
db2_last_check{Database="XXXDB"} 1.648820560020965e+09
# HELP db2_instance_info Information aboud the db2 instance
# TYPE db2_instance_info gauge
db2_instance_info{Database="XXXDB",Instance="db2inst1",Version="DB2 v11.5.6.0"} 1.0
# HELP db2_tablespace_size_kilobytes Total space
# TYPE db2_tablespace_size_kilobytes gauge
db2_tablespace_size_kilobytes{Database="XXXDB",Tablespace="SYSCATSPACE"} 1.998848e+06
db2_tablespace_size_kilobytes{Database="XXXDB",Tablespace="USERSPACE1"} 1.942355968e+09
db2_tablespace_size_kilobytes{Database="XXXDB",Tablespace="SYSTOOLSPACE"} 65536.0
db2_tablespace_size_kilobytes{Database="XXXDB",Tablespace="DSMSPACE"} 2.46874112e+08
db2_tablespace_size_kilobytes{Database="XXXDB",Tablespace="MONSPACE"} 4.194304e+06
db2_tablespace_size_kilobytes{Database="XXXDB",Tablespace="SPACE_RM_TS"} 7.55367936e+08
db2_tablespace_size_kilobytes{Database="XXXDB",Tablespace="SPACE_RA_TS"} 262144.0
db2_tablespace_size_kilobytes{Database="XXXDB",Tablespace="SPACE_PSS_TS"} 1.458700288e+09
db2_tablespace_size_kilobytes{Database="XXXDB",Tablespace="SPACE_TS"} 4.13630464e+08
db2_tablespace_size_kilobytes{Database="XXXDB",Tablespace="SPACEXX_STG_TS"} 6.276864e+07
db2_tablespace_size_kilobytes{Database="XXXDB",Tablespace="SPACEYY_STG_TS"} 3.3030144e+08
db2_tablespace_size_kilobytes{Database="XXXDB",Tablespace="SPACEZZ_TS"} 720896.0
db2_tablespace_size_kilobytes{Database="XXXDB",Tablespace="SPACEXX_TS"} 1.29499136e+09
db2_tablespace_size_kilobytes{Database="XXXDB",Tablespace="SPACEYY_TS"} 9.44963584e+08
db2_tablespace_size_kilobytes{Database="XXXDB",Tablespace="TMPSPACE"} 2.1415424e+07
# HELP db2_tablespace_used_kilobytes Space used
# TYPE db2_tablespace_used_kilobytes gauge
db2_tablespace_used_kilobytes{Database="XXXDB",Tablespace="SYSCATSPACE"} 1.97504e+06
db2_tablespace_used_kilobytes{Database="XXXDB",Tablespace="USERSPACE1"} 8.84095616e+08
db2_tablespace_used_kilobytes{Database="XXXDB",Tablespace="SYSTOOLSPACE"} 57600.0
db2_tablespace_used_kilobytes{Database="XXXDB",Tablespace="DSMSPACE"} 2.46866048e+08
db2_tablespace_used_kilobytes{Database="XXXDB",Tablespace="MONSPACE"} 15872.0
db2_tablespace_used_kilobytes{Database="XXXDB",Tablespace="SPACE_RM_TS"} 5.41412352e+08
db2_tablespace_used_kilobytes{Database="XXXDB",Tablespace="SPACE_RA_TS"} 8448.0
db2_tablespace_used_kilobytes{Database="XXXDB",Tablespace="SPACE_PSS_TS"} 1.408568576e+09
db2_tablespace_used_kilobytes{Database="XXXDB",Tablespace="SPACE_TS"} 4.13315584e+08
db2_tablespace_used_kilobytes{Database="XXXDB",Tablespace="SPACEXX_STG_TS"} 3.66496e+07
db2_tablespace_used_kilobytes{Database="XXXDB",Tablespace="SPACEYY_STG_TS"} 1.2024192e+07
db2_tablespace_used_kilobytes{Database="XXXDB",Tablespace="SPACEZZ_TS"} 544896.0
db2_tablespace_used_kilobytes{Database="XXXDB",Tablespace="SPACEXX_TS"} 1.294980864e+09
db2_tablespace_used_kilobytes{Database="XXXDB",Tablespace="SPACEYY_TS"} 9.44943232e+08
db2_tablespace_used_kilobytes{Database="XXXDB",Tablespace="TMPSPACE"} 2.1415296e+07
# HELP db2_bufferpool_currentsize_pages Current Size of Buffer Pool
# TYPE db2_bufferpool_currentsize_pages gauge
db2_bufferpool_currentsize_pages{Bufferpool="IBMDEFAULTBP",Database="XXXDB"} 3.208136e+06
# HELP db2_bufferpool_pagesize_bytes Page size of Buffer Pool
# TYPE db2_bufferpool_pagesize_bytes gauge
db2_bufferpool_pagesize_bytes{Bufferpool="IBMDEFAULTBP",Database="XXXDB"} 32768.0
# HELP db2_bufferpool_lreads_total_count Total logical reads in the bufferpool
# TYPE db2_bufferpool_lreads_total_count gauge
db2_bufferpool_lreads_total_count{Bufferpool="IBMDEFAULTBP",Database="XXXDB"} 1.0206423935e+010
# HELP db2_bufferpool_preads_total_count Total physical reads in the bufferpool
# TYPE db2_bufferpool_preads_total_count gauge
db2_bufferpool_preads_total_count{Bufferpool="IBMDEFAULTBP",Database="XXXDB"} 4.9599449e+07
# HELP db2_bufferpool_hitratio_total_percent Total hit ratio
# TYPE db2_bufferpool_hitratio_total_percent gauge
db2_bufferpool_hitratio_total_percent{Bufferpool="IBMDEFAULTBP",Database="XXXDB"} 99.97
# HELP db2_bufferpool_lreads_data_count Data logical reads in the bufferpool
# TYPE db2_bufferpool_lreads_data_count gauge
db2_bufferpool_lreads_data_count{Bufferpool="IBMDEFAULTBP",Database="XXXDB"} 9.333885693e+09
# HELP db2_bufferpool_preads_data_count Data physical reads in the bufferpool
# TYPE db2_bufferpool_preads_data_count gauge
db2_bufferpool_preads_data_count{Bufferpool="IBMDEFAULTBP",Database="XXXDB"} 4.8490705e+07
# HELP db2_bufferpool_hitratio_data_percent Data hit ratio
# TYPE db2_bufferpool_hitratio_data_percent gauge
db2_bufferpool_hitratio_data_percent{Bufferpool="IBMDEFAULTBP",Database="XXXDB"} 99.98
# HELP db2_bufferpool_lreads_index_count Index logical reads in the bufferpool
# TYPE db2_bufferpool_lreads_index_count gauge
db2_bufferpool_lreads_index_count{Bufferpool="IBMDEFAULTBP",Database="XXXDB"} 7.64079785e+08
# HELP db2_bufferpool_preads_index_count Index physical reads in the bufferpool
# TYPE db2_bufferpool_preads_index_count gauge
db2_bufferpool_preads_index_count{Bufferpool="IBMDEFAULTBP",Database="XXXDB"} 1.108744e+06
```
